<?php
/**
 * Plugin Name: Import products to WooCommerce
 * Plugin URI:
 * Description: Import products to WooCommerce from specific XML-file
 * Version: 1.00
 * Author: Alex Romantsov
 * Author URI:
 * Text Domain: wc_addproduct
 * Domain Path: /languages
 * License: No Licence
 */


function register_my_custom_submenu_page() {
    add_submenu_page(
        'woocommerce',
        __('Import products', 'wc_addproduct'),
        __('Import products', 'wc_addproduct'),
        'manage_options',
        'woo-import-products',
        'my_custom_submenu_page_callback'
    );
}
add_action('admin_menu', 'register_my_custom_submenu_page');


function wc_addproduct_load_plugin_textdomain() {
    load_plugin_textdomain( 'wc_addproduct', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'wc_addproduct_load_plugin_textdomain' );


// Get XML, parse and update products
function addProduct($xml){
    global $wpdb;

    $count = null;
    $products = simplexml_load_file($xml);

    foreach ($products->product as $product) {

        $count++;

        $post = array(
            'post_title' => (string)$product->title,
            'post_content' => (string)$product->content,
            'post_status' => "publish",
            'post_excerpt' => (string)$product->content,
            'post_name' => sanitize_title((string)$product->title), //name/slug
            'post_type' => "product"
        );

        $post_info = get_page_by_title( $post['post_title'], OBJECT, 'product' );
        if (!empty($post_info)) {
            $new_post_id = wp_update_post( $post_info );
        } else {
            $new_post_id = wp_insert_post($post);
        }

        if (isset($product->categories->category)) {
            // Create the category
            $cat = wp_insert_term(
                (string)$product->categories->category[0],
                'product_cat', // the taxonomy
                array(
                    'description' => '',
                    'slug' => sanitize_title((string)$product->categories->category['slug']),
                )
            );

            if (!is_wp_error($cat) && isset($cat['term_taxonomy_id'])) {
                $catId = $cat['term_id'];
            } else {
                $catId = get_term_by('slug', sanitize_title((string)$product->categories->category['slug']), 'product_cat');
                $catId = $catId->term_id;
            }
        } else {
            $cat = NULL;
        }

        //make product type be variable:
        wp_set_object_terms($new_post_id, 'simple', 'product_type');
        //add category to product:
        if (!empty($cat)) {
            wp_set_object_terms($new_post_id, $catId, 'product_cat');
        }
        update_post_meta($new_post_id, '_visibility', 'visible');
        update_post_meta($new_post_id, '_stock_status', (string)$product->stock_status);
        update_post_meta($new_post_id, 'total_sales', '0');
        update_post_meta($new_post_id, '_downloadable', 'no');
        update_post_meta($new_post_id, '_virtual', 'no');
        update_post_meta($new_post_id, '_price', (string)$product->regular_price);
        update_post_meta($new_post_id, '_regular_price', (string)$product->regular_price);
        update_post_meta($new_post_id, '_sale_price', (string)$product->sale_price);
        update_post_meta($new_post_id, '_purchase_note', "");
        update_post_meta($new_post_id, '_featured', "no");
        update_post_meta($new_post_id, '_weight', "");
        update_post_meta($new_post_id, '_length', "");
        update_post_meta($new_post_id, '_width', "");
        update_post_meta($new_post_id, '_height', "");
        update_post_meta($new_post_id, '_sku', sanitize_title((string)$product->title));

        // manage product media (images)
        if (isset($product->images->image)) {
            delete_post_media($new_post_id);
            foreach($product->images->image as $key => $image) {
                if (isset( $image['big'] )) {
                    add_img($new_post_id, (string)$image, true);
                } else add_img($new_post_id, (string)$image, false);
            }
        }
    }

    return $count;
}//end addProduct function


function my_custom_submenu_page_callback() {
    $bytes = apply_filters( 'import_upload_size_limit', wp_max_upload_size() );
    $size = size_format( $bytes );
    $upload_dir = wp_upload_dir();
    ?>
    <div id='wp-product-upload-container'>
    <?php if ( !empty( $upload_dir['error'] ) ) : ?>
        <div class="error">
            <p><?php _e('Before you can upload your import file, you will need to fix the following error:'); ?></p>
            <p>
                <strong>
                    <?php echo $upload_dir['error']; ?>
                </strong>
            </p>
        </div>
        <?php else : ?>
        <h3>
            <?php _e('Import products', 'wc_addproduct'); ?>
        </h3>
        <form enctype="multipart/form-data" id="wp-upload-form" method="post" class="wp-upload-form" action="">
            <p>
                <label for="import">
                    <?php _e( 'Choose a file from your computer:' ); ?>
                </label> (<?php printf( __('Maximum size: %s' ), $size ); ?>)
                <input type="file" id="import" name="import" size="25" />
                <input type="hidden" name="action" value="save" />
                <input type="hidden" name="max_file_size" value="<?php echo $bytes; ?>" />
            </p>
            <?php submit_button( __('Upload file and import'), 'button-primary', 'wp-upload-submit'); ?>
        </form>
    </div>
        <div id='wp-product-upload-message'>
        </div>
        <div id='loader' style='display: none;'>
            <div class='loader'></div>
                <h3>
                    <?php _e('Please wait...', 'wc_addproduct'); ?>
                </h3>
        </div>
        <?php
    endif;
}

//add product image:
function add_img($post_id, $link, $thumb) {

    require_once(ABSPATH . 'wp-admin/includes/file.php');
    require_once(ABSPATH . 'wp-admin/includes/media.php');
    require_once(ABSPATH . "wp-admin" . '/includes/image.php');

    if ($thumb) {
        // Download file to temp location
        $tmp = download_url( $link );

        // Set variables for storage
        // fix file name for query strings
        preg_match('/[^\?]+\.(jpg|JPG|jpe|JPE|jpeg|JPEG|gif|GIF|png|PNG)/', $link, $matches);
        $file_array['name'] = basename($matches[0]);
        $file_array['tmp_name'] = $tmp;
        // If error storing temporarily, unlink
        if ( is_wp_error( $tmp ) ) {
            @unlink($file_array['tmp_name']);
            $file_array['tmp_name'] = '';
        }
        //use media_handle_sideload to upload img:
        $thumbid = media_handle_sideload( $file_array, $post_id, 'product thumbhail' );

        // If error storing permanently, unlink
        if ( is_wp_error($thumbid) ) {
            @unlink($file_array['tmp_name']);
        //return $thumbid;
        }
        set_post_thumbnail($post_id, $thumbid);
    } else {
        // Download file to temp location
        $tmp = download_url( $link );
        // fix file name for query strings
        preg_match('/[^\?]+\.(jpg|JPG|jpe|JPE|jpeg|JPEG|gif|GIF|png|PNG)/', $link, $matches);
        $file_array['name'] = basename($matches[0]);
        $file_array['tmp_name'] = $tmp;
        $imgID = media_handle_sideload( $file_array, $post_id, 'product image' );

        update_post_meta( $post_id, '_product_image_gallery', $imgID);
    }
}

// Delete all attached to post media
function delete_post_media($post_id) {

    $attachments = get_posts( array(
        'post_type'      => 'attachment',
        'posts_per_page' => -1,
        'post_status'    => 'any',
        'post_parent'    => $post_id
    ) );

    foreach ( $attachments as $attachment ) {
        wp_delete_attachment( $attachment->ID);
    }
}

function ajax_wc_xml_load_enqueuer() {
     // code to embed th  java script file that makes the Ajax request
    wp_enqueue_script( 'ajax_wc_xml_load', plugins_url( 'js/script.js', __FILE__ ), array('jquery'), '1.0', true );
    wp_localize_script( 'ajax_wc_xml_load', 'ajax_data', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' )
    ) );
    wp_enqueue_style( 'wc_xml_style', plugins_url('style.css', __FILE__));
}
add_action( 'admin_enqueue_scripts', 'ajax_wc_xml_load_enqueuer');

// AJAX Callback function. Getting here a file from FORM, and calling main function for XML parsing. Return's a message with results.
function my_wc_xml_load() {
    // Getting path to uploaded file
    $upload_dir = wp_upload_dir();
    $target_path = $upload_dir['basedir'];
    $target_path = $target_path .'/'. basename( $_FILES['import']['name']);

    // Define a AJAX-response array
    $count = 0;
    $response = array(
        'count' => $count,
        'message' => ''
    );

    // Check filetype
    if ($_FILES['import']['type'] == 'text/xml') {
        // Copy file to locale
        if (move_uploaded_file($_FILES['import']['tmp_name'], $target_path)) {
            //If success, exec main XML-parse function
            $count = addProduct($target_path);
        } else $message = __('There was an error uploading the file, please try again!', 'wc_addproduct');
    } else $message = __('Wrong file type! Please, choose *.XML', 'wc_addproduct');

    $response['count'] = $count;

    // Fill-up message for AJAX-response
    if ($message == '') {
        if ($count > 0) {
            $message = __('Products has been imported! Amount:', 'wc_addproduct');

        } else {
            $message = __('Products in XML has not been founded', 'wc_addproduct');
        }
    }

    // Send json for AJAX...
    $response['message'] = $message;
    echo json_encode($response);
    die();
}

add_action( 'wp_ajax_nopriv_ajax_wc_xml_load', 'my_wc_xml_load' );
add_action( 'wp_ajax_ajax_wc_xml_load', 'my_wc_xml_load' );


?>

