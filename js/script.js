(function($) {

    $(document).on( 'click', '#wp-upload-submit', function( event ) {
        event.preventDefault();

        var datasend = new FormData();
        datasend.append( 'import', $('#import')[0].files[0] ); //photo is the name and id of the <input type="file">
        datasend.append( 'action', 'ajax_wc_xml_load');

        $.ajax({
            url: ajax_data.ajaxurl,
            type: 'post',
            cache: false,
            processData: false,
            contentType: false,
            data : datasend,
            beforeSend: function(){
                $('#wp-product-upload-message').removeClass();
                $('#wp-product-upload-message').hide();
                $('#loader').show();
            },
            success: function( response ) {

                var data = JSON.parse(response);

                $('#loader').hide();

                    if (data.count > 0) {
                        $('#wp-product-upload-message').html('<h2>'+data.message+' '+data.count+'</h2>');
                        $('#wp-product-upload-message').addClass( "updated" );
                        $('#wp-product-upload-message').show();
                        document.getElementById("wp-upload-form").reset();
                        document.getElementById("wp-upload-submit").disabled = true;
                    } else {
                        if (data.message) $('#wp-product-upload-message').html('<h2>'+data.message+'</h2>');
                        else $('#wp-product-upload-message').html('<h2>Failure!</h2>');
                        $('#wp-product-upload-message').addClass("error");
                        $('#wp-product-upload-message').show();
                    }

            }
        })
    })
})(jQuery);